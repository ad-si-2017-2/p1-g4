# APLICAÇÕES DISTRIBUÍDAS - INF/UFG - 17/2
Repositorio para a disciplina de Aplicações Distribuídas do curso de Sistemas de Informação da Universidade Federal de Goiás.
## Projeto 1 - Servidor IRC utilizando sockets em nodejs
### MEMBROS
* [Yago Zardo](https://gitlab.com/yagozardo) - Líder do Grupo e Desenvolvedor
* [Murillo Vieira](https://gitlab.com/murilloves) - Desenvolvedor
* [Renan Gustavo](https://gitlab.com/Vouks) - Documentador

### Documentação
Toda a documentação deste projeto pode ser acompanhada pela [wiki](https://gitlab.com/ad-si-2017-2/p1-g4/wikis/home).

### Requisitos
- Atendimento aos padrões das RFCs do protocolo;
- Suportar comunicação básica entre dois clientes diferentes;
- Suporte total ou maioria das mensagens:
    - Registro de conexão;
    - Operação de canal;
    - Envio de mensagens;
    - Consultas ao servidor;
    - Consultas baseadas no usuário;
    - Variadas.

### Entregáveis Desejáveis:
* Manual do usuário: informando como utilizar o servidor; requisitos de hardware e software; como instalar, testar e configurar;
* Manual do desenvolvedor: lista de cada mensagem suportada, referencias (RFC), testes de execução (procedimentos em telnet, resultado esperado); lista de mensagens não suportadas; diagramas de sequencia, diagramas de atividade.
* Código-fonte do sistema com documentação de casos de testes.