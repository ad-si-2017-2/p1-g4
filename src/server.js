const net = require('net');

const message = require('./message')
const commands = require('./commands');
const configuration = require('./configuration');
const sessionService = require('./session');

const mapCommands = new Map();
const startDate = new Date();

mapCommands.set('PASS', commands.pass);
mapCommands.set('JOIN', commands.join);
mapCommands.set('KICK', commands.kick);
mapCommands.set('INVITE', commands.invite);
mapCommands.set('NICK', commands.nick);
mapCommands.set('USER', commands.user);
mapCommands.set('PING', commands.ping);
mapCommands.set('PONG', commands.pong);
mapCommands.set('MODE', commands.mode);
mapCommands.set('QUIT', commands.quit);
mapCommands.set('PROTOCTL', commands.protoctl);
mapCommands.set('PRIVMSG', commands.privmsg);

net.createServer(socket => {
    const session = sessionService.createSession(socket);
    socket.on('data', (data) => {
        const dataMessage = new String(data);
        const splittedMessage = dataMessage.split(configuration.commandSeparator);
        const command = splittedMessage[0].trim().toUpperCase();
        splittedMessage.splice(0, 1);
        const args = splittedMessage.join(configuration.commandSeparator).trim();
        const commandToBeExecuted = mapCommands.get(command) || commandNotFound(command);
        commandToBeExecuted(session, args);
    });
    socket.on('close', () => {
        sessionService.destroySession(session);
    });
}).listen(configuration.port);

//função para indentificar os commands nao identificados, seguindo a RFC, passando o status code
function commandNotFound(command) {
    return (session, args) => {
        message.send(session, {
            status: message.status.ERR_UNKNOWNCOMMAND,
            message: `${command}`
        })
    }
}

module.exports.start_date = startDate;

console.info(`Server initialized on port ${configuration.port} at ${startDate}`);