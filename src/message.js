function send(session, options) {
    if (options.status) {
        session.socket.write(`:${session.name} ${options.status} ${options.message}\r\n`);
    } else {
        session.socket.write(`${options.message}\r\n`);
    }
}

module.exports.send = send;
module.exports.status = {
    RPL_WELCOME: '001',
    RPL_YOURHOST: '002',
    RPL_CREATED: '003',
    RPL_MYINFO: '004',
    ERR_NEEDMOREPARAMS: '461',
    ERR_ALREADYREGISTRED: '462',
    ERR_NICKNAMEINUSE: '433',
    ERR_UNKNOWNCOMMAND: '421',
    ERR_NOSUCHNICK: '401'
};