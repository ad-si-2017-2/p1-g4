const server = require('./server');
const user = require('./user');
const configuration = require('./configuration');
const message = require('./message');

function nickCommand(session, nick) {
    user.setNick(session, nick);
}

function kickCommand(session, args) {

}

function userCommand(session, args) {
    if (args) {
        const values = args.split(' ');
        if (values.length < 4) {
            message.send(session, {
                status: message.status.ERR_NEEDMOREPARAMS,
                message: `USER :Not enough parameters`
                    //validacao inicial de user qnt a numeros de parametros
            });
        } else {
            const values = args.split(':');
            const options = values[0].trim().split(' ');
            const nameOption = values[1].trim();
            const userOption = options[0];
            const modeOption = options[1];
            const unussedOption = options[2];
            // TODO: Parse mode and unussed
            user.setUser(session, nameOption, userOption, modeOption, unussedOption);
            message.send(session, { status: message.status.RPL_WELCOME, message: `${session.user.nick} :Welcome to the Internet Relay Network ${session.user.nick}!${session.user.user}@${session.name}` });
            message.send(session, { status: message.status.RPL_YOURHOST, message: `Your host is ${session.name}, running version ${session.version}` });
            message.send(session, { status: message.status.RPL_CREATED, message: `This server was created ${server.start_date}` });
        }
    } else {
        message.send(session, {
            status: message.status.ERR_NEEDMOREPARAMS,
            message: `USER :Not enough parameters`
        });
    }
}

function inviteCommand(session, args) {

}

function joinCommand(session, args) {

}

function passCommand(session, password) {
    user.setSessionPassword(session, password);
}

function pongCommand(session, args) {

}

function pingCommand(session, args) {

}

function modeCommand(session, args) {

}

function quitCommand(session, args) {

}

function protoctlCommand(session, args) {

}

function privmsgCommand(session, args) {
    const splittedArgs = args.split(' ');
    const argsUser = splittedArgs[0].trim();
    const argsMessage = splittedArgs.slice(1, splittedArgs.length);
    user.sendMessage(session, argsUser, argsMessage);
}

function validateArgs(args) {
    // TODO: validate if the args is present
    // If is true, the command can be processed and call specific validations
    // If is false, the command can't be processed and emit to client that has invalid parameters
}

module.exports.kick = kickCommand;
module.exports.invite = inviteCommand;
module.exports.join = joinCommand;
module.exports.pass = passCommand;
module.exports.nick = nickCommand;
module.exports.user = userCommand;
module.exports.ping = pongCommand;
module.exports.pong = pingCommand;
module.exports.mode = modeCommand;
module.exports.quit = quitCommand;
module.exports.protoctl = protoctlCommand;
module.exports.privmsg = privmsgCommand;