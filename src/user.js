const message = require('./message');
const server = require('./server');

const userMap = new Map();

module.exports.sendMessage = (session, user, userMessage) => {
    const sessionDestination = userMap.get(user);
    if (sessionDestination) {
        message.send(sessionDestination, {
            message: `:${session.user.nick}!${session.user.user}@${session.name} PRIVMSG ${user} :${userMessage}`
        });
    } else {
        console.log(`ERR_NOSUCHNICK`);
        message.send(session, {
            // `:${session.name} ${} :No such nick/channel`
        })
    }
};

module.exports.setUser = (session, name, user, mode, unussed) => {
    session.user.name = name;
    session.user.user = user;
    session.user.mode = mode;
    session.user.unussed = unussed;
};

module.exports.setNick = (session, nick) => {
    const sessionFromNick = userMap.get(nick);
    if (sessionFromNick) {
        if (sessionFromNick.id === session.id) {
            session.user.nick = nick;
        } else {
            if (sessionFromNick.password === session.password) {
                if (sessionFromNick.password) {
                    session.user.nick = nick;
                } else {
                    message.send(session, {
                        status: message.status.ERR_NICKNAMEINUSE,
                        message: `${nick} :${nick}`
                    });
                }
            } else {
                message.send(session, {
                    status: message.status.ERR_NICKNAMEINUSE,
                    message: `${nick} :${nick}`
                });
            }
        }
    } else {
        let previousNick = session.user.nick;
        userMap.delete(session.user.nick);
        session.user.nick = nick;
        userMap.set(nick, session);
        if (previousNick) {
            message.send(session, { message: `:${previousNick}!${session.user.name}@${session.user.user} NICK :${nick}` });
        }
    }
}

module.exports.setSessionPassword = (session, password) => {
    session.user.password = password;
};

module.exports.removeUserSession = (session) => {
    userMap.delete(session.user.nick);
};