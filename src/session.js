const uuid = require('uuid/v4');
const package = require('../package.json');

const user = require('./user');

const sessionMap = new Map();

module.exports.createSession = (socket) => {
    const id = uuid();
    const session = {
        id: id,
        enter_at: new Date(),
        socket: socket,
        name: `${socket.remoteAddress}:${socket.remotePort}`,
        version: package.version,
        user: {
            nick: undefined,
            user: undefined,
            password: undefined,
            name: undefined,
            mode: undefined,
            unussed: undefined
        }
    };
    sessionMap.set(id, session);
    return session;
}

module.exports.destroySession = (session) => {
    sessionMap.delete(session.id);
    user.removeUserSession(session);
}